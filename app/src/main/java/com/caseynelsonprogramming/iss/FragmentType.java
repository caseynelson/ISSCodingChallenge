package com.caseynelsonprogramming.iss;

/**
 * Fragment type can show either space station pass data or astronauts on the station currently
 */

public enum FragmentType {
    STATIONPASS,
    STATIONSTAFF
}

package com.caseynelsonprogramming.iss.networking;

import com.caseynelsonprogramming.iss.model.Request;
import com.caseynelsonprogramming.iss.service.IssEndpointClient;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 *  Uses Retrofit2 as a Type-safe HTTP Client to builds a retrofit object
 *  and connects it to our apis that we want to reach
 */

public class Retrofit2Client {

    private static Retrofit2Client instance = null;
    public static final String BASE_URL = "http://api.open-notify.org/";
    private Retrofit retrofitBuilder;

    //Keep services here and build in retrofit later
    private IssEndpointClient issEndpointClient;

    //Only need 1 instance of Retrofit so singleton pattern used
    public static synchronized Retrofit2Client getInstance(){
        if (instance == null){
            instance = new Retrofit2Client();
        }

        return  instance;
    }

    private Retrofit2Client(){
        retrofitBuilder = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

        this.issEndpointClient = retrofitBuilder.create(IssEndpointClient.class);
    }

    public IssEndpointClient getIssEndpointClient() {
        return issEndpointClient;
    }

    public Retrofit getRetrofitBuilder() {
        return retrofitBuilder;
    }


}

package com.caseynelsonprogramming.iss.ui;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.caseynelsonprogramming.iss.FragmentType;
import com.caseynelsonprogramming.iss.IssClientAdapter;
import com.caseynelsonprogramming.iss.IssDataCallback;
import com.caseynelsonprogramming.iss.ListItem;
import com.caseynelsonprogramming.iss.R;
import com.caseynelsonprogramming.iss.data.IssData;
import com.caseynelsonprogramming.iss.model.Astronaut;
import com.caseynelsonprogramming.iss.model.Response;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import java.util.ArrayList;
import java.util.List;

public class SpaceStationDetailsFragment extends Fragment implements IssDataCallback {

    private static final String TITLE_TEXT = "title text";
    private static final String FRAGMENT_TYPE = "fragment type";

    private TextView titleText;
    private String stationInfoTitle;
    private RecyclerView recyclerView;
    private FragmentType fragmentType;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private IssData data;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION_CODE = 200;
    View rootView;

    public SpaceStationDetailsFragment(){
        //required empty constructor
    }

    public static SpaceStationDetailsFragment newInstance(String titleText, FragmentType fragmentType){
        SpaceStationDetailsFragment fragment = new SpaceStationDetailsFragment();

        //Create Bundle and put in data if need be
        Bundle args = new Bundle();
        args.putString(TITLE_TEXT, titleText);
        args.putSerializable(FRAGMENT_TYPE, fragmentType);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        if (getArguments() != null) {
            stationInfoTitle = getArguments().getString(TITLE_TEXT);
            fragmentType = (FragmentType)getArguments().getSerializable(FRAGMENT_TYPE);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_space_station_info, container,false);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        data = new IssData(this, getActivity());

        titleText = rootView.findViewById(R.id.rv_header_text);
        titleText.setText(stationInfoTitle);

        //snackView = rootView.findViewById(R.id.main_activity_rootview);

        //Setup recycler view
        recyclerView = rootView.findViewById(R.id.iss_data_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //check for permissions for accessing their location
        checkPermissions();

        return  rootView;
    }

    public void requestPermissions(){
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                MY_PERMISSIONS_REQUEST_LOCATION_CODE);
    }

    /**
     * Check to see if we have proper permissions to acquire location, if not ask for it
     */
    public void checkPermissions(){

        //Check if we have permission, if we don't have it, request it
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
        } else {
            //permission granted
            loadIssData();

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Permission granted, load data
                    loadIssData();

                } else {

                    //Should we show an explanation?
                    if(ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)){
                        Snackbar.make(getActivity().findViewById(R.id.main_activity_rootview), "We need your location to show you where to see the ISS!", Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestPermissions();
                            }
                        }).show();
                    }

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
         
        }
    }

    private void loadIssData() {
        // permission was granted. Do the appropriate network calls
        fusedLocationProviderClient.getLastLocation()
                .addOnSuccessListener( getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        //got last known location, rare situations it can be null
                        if(location != null){
                            if(fragmentType == FragmentType.STATIONPASS){

                                data.getIssFlightData(location.getLatitude(), location.getLongitude());
                            }else if(fragmentType == FragmentType.STATIONSTAFF){
                                data.getAstronautData();
                            }
                        }
                        else{
                            Toast.makeText(getActivity(), "Location can't be find, try again later", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    @Override
    public void onIssDataReady(List<Response> responseList) {
        List<ListItem> list = new ArrayList<>();
        list.addAll(responseList);

        recyclerView.setAdapter(new IssClientAdapter(list));
    }

    @Override
    public void onIssAstronautsReady(List<Astronaut> astroList) {
        List<ListItem> list = new ArrayList<>();
        list.addAll(astroList);

        recyclerView.setAdapter(new IssClientAdapter(list));
    }

    @Override
    public void onResponseError(String errorReason) {
        Toast.makeText(getActivity(), errorReason, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNetworkFailure() {
        Toast.makeText(getActivity(), "No network connection", Toast.LENGTH_SHORT).show();

    }


}

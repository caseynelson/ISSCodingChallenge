package com.caseynelsonprogramming.iss;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class ViewHolder extends RecyclerView.ViewHolder{


    public ViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bindType(ListItem item);
}

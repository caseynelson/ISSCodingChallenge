package com.caseynelsonprogramming.iss;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.caseynelsonprogramming.iss.ui.SpaceStationDetailsFragment;

/**
 *
 */
public class IssPagerAdapter extends FragmentPagerAdapter {

    private static int FRAGMENT_COUNT = 2;
    private Context context;
    private String[] title_array;

    public IssPagerAdapter(FragmentManager fm, Context context, String[] titles) {
        super(fm);
        this.context = context;
        title_array = titles;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return SpaceStationDetailsFragment.newInstance(title_array[position],FragmentType.STATIONPASS);
            case 1:
                return SpaceStationDetailsFragment.newInstance(title_array[position],FragmentType.STATIONSTAFF);
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return FRAGMENT_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position){
        switch (position){
            case 0:
                return context.getString(R.string.pass_times);
            case 1:
                return context.getString(R.string.astronauts);
        }
        return null;

    }
}

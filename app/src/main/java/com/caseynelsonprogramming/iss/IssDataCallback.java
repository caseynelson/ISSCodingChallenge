package com.caseynelsonprogramming.iss;

import com.caseynelsonprogramming.iss.model.Astronaut;
import com.caseynelsonprogramming.iss.model.Response;

import java.util.List;

/**
 * Group of methods that handle callbacks once ISS data is retrieved
 */

public interface IssDataCallback {

    void onIssDataReady(List<Response> responseList);

    void onIssAstronautsReady(List<Astronaut> astroList);

    void onResponseError(String errorReason);

    void onNetworkFailure();



}

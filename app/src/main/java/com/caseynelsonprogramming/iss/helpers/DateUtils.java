package com.caseynelsonprogramming.iss.helpers;


public class DateUtils {


    /**
     * Converts an epoch time stamp to a human readable format
     * @param epoch epoch time stamp
     * @return date in human readable format
     */
    public static String convertEpochToString(long epoch){

        return new java.text.SimpleDateFormat("EEEE MMMM dd yyyy h:mm a").format(new java.util.Date (epoch *1000));


    }
}

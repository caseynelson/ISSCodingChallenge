package com.caseynelsonprogramming.iss.helpers;


import com.caseynelsonprogramming.iss.model.ApiError;
import com.caseynelsonprogramming.iss.networking.Retrofit2Client;
import java.io.IOException;
import java.lang.annotation.Annotation;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Util class that takes parses an error response into an ApiError class
 */

public class ErrorUtils {

    public static ApiError parseError(Response<?> response){
        ApiError error;

        Converter<ResponseBody, ApiError> converter = Retrofit2Client.getInstance().getRetrofitBuilder().responseBodyConverter(ApiError.class, new Annotation[0]);

        try{
            error = converter.convert(response.errorBody());
        }catch (IOException e){
            return new ApiError();
        }

        return error;
    }
}

package com.caseynelsonprogramming.iss.model;

public class ApiError {


    private String message;
    private String reason;


    public String getMessage() {
        return message;
    }

    public String getReason() {
        return reason;
    }
}

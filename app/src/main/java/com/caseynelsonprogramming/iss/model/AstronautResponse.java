package com.caseynelsonprogramming.iss.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class AstronautResponse {

    @SerializedName("number")
    private int number;

    @SerializedName("people")
    private List<Astronaut> astronautList;

    @SerializedName("message")
    private String message;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public List<Astronaut> getAstronautList() {
        return astronautList;
    }

    public void setAstronautList(List<Astronaut> astronautList) {
        this.astronautList = astronautList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

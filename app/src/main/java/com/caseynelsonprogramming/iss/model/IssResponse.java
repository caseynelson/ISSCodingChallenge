package com.caseynelsonprogramming.iss.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class IssResponse {

    @SerializedName("message")
    private String message;

    @SerializedName("request")
    private Request request;

    @SerializedName("response")
    private List<Response> responseList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public List<Response> getResponseList() {
        return responseList;
    }

    public void setResponse(List<Response> response) {
        this.responseList = response;
    }


}

package com.caseynelsonprogramming.iss.model;

import com.caseynelsonprogramming.iss.ListItem;
import com.google.gson.annotations.SerializedName;


public class Astronaut implements ListItem {

    @SerializedName("craft")
    private String craft;

    @SerializedName("name")
    private String name;

    public String getCraft() {
        return craft;
    }

    public void setCraft(String craft) {
        this.craft = craft;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getListItemType() {
        return ListItem.ASTROITEM;
    }
}

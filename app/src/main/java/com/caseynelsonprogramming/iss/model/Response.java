package com.caseynelsonprogramming.iss.model;

import com.caseynelsonprogramming.iss.ListItem;
import com.google.gson.annotations.SerializedName;



public class Response implements ListItem {

    @SerializedName("duration")
    private int duration;

    @SerializedName("risetime")
    private long risetime;

    public int getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public long getRisetime() {
        return risetime;
    }

    public void setRisetime(Integer risetime) {
        this.risetime = risetime;
    }

    @Override
    public int getListItemType() {
        return ListItem.PASSITEM;
    }
}

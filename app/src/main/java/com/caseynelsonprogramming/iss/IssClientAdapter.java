package com.caseynelsonprogramming.iss;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.caseynelsonprogramming.iss.helpers.DateUtils;
import com.caseynelsonprogramming.iss.model.Astronaut;
import com.caseynelsonprogramming.iss.model.Response;



import java.util.List;

/**
 * Reusing recycler view so adjusting viewholders to handle multiple row types
 * Also in future if we want more then 1 type of view in list, this will scale for that
 */

public class IssClientAdapter extends RecyclerView.Adapter<ViewHolder> {
    private List<ListItem> list;
    private Context context;

    public IssClientAdapter(List<ListItem> list) {
        this.list = list;

    }
    //Adapter should only connect RecyclerView and it's data via the view holder
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View issInfoView = inflater.inflate(R.layout.item_iss_pass_details, parent, false);

        switch (viewType){
            case ListItem.PASSITEM:
                return new PassTimeViewHolder(issInfoView);
            case ListItem.ASTROITEM:
                return new AstroViewHolder(issInfoView);
        }

        return null;
    }

    //This method is called for each ViewHolder to bind it to the adapter. This is where we will pass our data to our ViewHolder.
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ListItem item = list.get(position);
        holder.bindType(item);

    }

    @Override
    public int getItemViewType(int position){
        return  list.get(position).getListItemType();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private class PassTimeViewHolder extends ViewHolder {
        TextView duration;
        TextView passTime;

        public PassTimeViewHolder(View itemView) {
            super(itemView);
            duration = itemView.findViewById(R.id.text_duration);
            passTime = itemView.findViewById(R.id.text_pass_time);

        }

        @Override
        public void bindType(ListItem item){
            Response obj = (Response)item;
            duration.setText(String.format("ISS visible for: %s seconds", String.valueOf(obj.getDuration())));
            passTime.setText(DateUtils.convertEpochToString(obj.getRisetime()));

        }
    }

    private class AstroViewHolder extends ViewHolder{
        TextView craft;
        TextView name;

        public AstroViewHolder(View itemView) {
            super(itemView);
            craft = itemView.findViewById(R.id.text_duration);
            name = itemView.findViewById(R.id.text_pass_time);
        }

        @Override
        public void bindType(ListItem item) {
            Astronaut obj = (Astronaut)item;
            craft.setText(String.format("Current space craft: %s", String.valueOf(obj.getCraft())));
            name.setText(String.format("Astronaut: %s", String.valueOf(obj.getName())));
        }
    }
}

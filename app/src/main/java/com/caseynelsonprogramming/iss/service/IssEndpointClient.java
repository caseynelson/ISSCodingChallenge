package com.caseynelsonprogramming.iss.service;

import com.caseynelsonprogramming.iss.model.AstronautResponse;
import com.caseynelsonprogramming.iss.model.IssResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Api to access ISS endpoints
 */

public interface IssEndpointClient {
        /**
         * Retrieves when the ISS will be in your area based on your lat and lon
         * @param latitude current latitude
         * @param longitude current longitude
         * @return
         */
        @GET("iss-pass.json")
        Call<IssResponse> getIssTravelData(@Query("lat") double latitude, @Query("lon") double longitude);

        /**
         * Retrieves all the astronauts currently on the ISS
         * @return
         */
        @GET("astros.json")
        Call<AstronautResponse> getAstronautTravelData();

}

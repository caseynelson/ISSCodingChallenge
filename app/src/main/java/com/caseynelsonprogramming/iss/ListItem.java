package com.caseynelsonprogramming.iss;

/**
 * Recyclerview displaying data can hold more then one time of item, or if it is reused
 * this will allow us to scale if we want to add multiple types of views in the future
 */

public interface ListItem {

    int PASSITEM = 0;
    int ASTROITEM =1;

    int getListItemType();

}

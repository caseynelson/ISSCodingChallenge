package com.caseynelsonprogramming.iss.data;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.nfc.Tag;
import android.support.annotation.NonNull;
import android.util.Log;

import com.caseynelsonprogramming.iss.IssDataCallback;
import com.caseynelsonprogramming.iss.helpers.ErrorUtils;
import com.caseynelsonprogramming.iss.model.ApiError;
import com.caseynelsonprogramming.iss.model.Astronaut;
import com.caseynelsonprogramming.iss.model.AstronautResponse;
import com.caseynelsonprogramming.iss.model.IssResponse;
import com.caseynelsonprogramming.iss.networking.Retrofit2Client;
import com.caseynelsonprogramming.iss.service.IssEndpointClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Casey on 2/7/2018.
 */

public class IssData {

    private static final String TAG = IssData.class.getSimpleName();
    private static IssData instance;
    private IssDataCallback listener;
    private Context context;

    //private Retrofit2Client retrofit2Client;


    public IssData (IssDataCallback listener, Context context) {

        //retrofit2Client = Retrofit2Client.getInstance();
        this.listener = listener;
        this.context = context;

    }


    public void getIssFlightData(double latitude, double longitude){

        IssEndpointClient client = Retrofit2Client.getInstance().getIssEndpointClient();
        Call<IssResponse> call = client.getIssTravelData(latitude, longitude);

        //Execute service call asynchronously so it's off the main thread
        if(isOnline()) {
            call.enqueue(new Callback<IssResponse>() {
                @Override
                public void onResponse(Call<IssResponse> call, Response<IssResponse> response) {

                    //Server returns something but doesn't guarantee it's a successful response
                    if (response.isSuccessful()) {
                        Log.d(TAG, "Successful Request");
                        IssResponse issResponse = response.body();
                        listener.onIssDataReady(issResponse.getResponseList());
                    } else {
                        //Got response so on network level everything went correct but got some error from server, input could
                        //be incorrect or server is overloaded
                        //Don't want to show entire error of JSON reponse so we extract exact error message and show the user
                        Log.d(TAG, "Server response error");
                        ApiError error = ErrorUtils.parseError(response);
                        listener.onResponseError(error.getMessage() + ": " + error.getReason());

                    }
                }

                @Override
                public void onFailure(Call<IssResponse> call, Throwable t) {
                    //Network failure i.e. no internet, try to access server but device has no internet connection
                    Log.d(TAG, "No internet connection, response failed");
                    listener.onNetworkFailure();
                }
            });
        }

    }

    /*
    Make data call to retrieve all the astronauts on board the space station at the time of the call
     */
    public void getAstronautData(){

        IssEndpointClient client = Retrofit2Client.getInstance().getIssEndpointClient();
        Call<AstronautResponse> call = client.getAstronautTravelData();

        //Execute service call asynchronously so it's off the main thread
        if(isOnline()) {
            call.enqueue(new Callback<AstronautResponse>() {
                @Override
                public void onResponse(Call<AstronautResponse> call, Response<AstronautResponse> response) {

                    //Server returns something but doesn't guarantee it's a successful response
                    if (response.isSuccessful()) {
                        Log.d(TAG, "Successful Request");
                        AstronautResponse astroResponse = response.body();
                        listener.onIssAstronautsReady(astroResponse.getAstronautList());
                    } else {
                        //Got response so on network level everything went correct but got some error from server, input could
                        //be incorrect or server is overloaded
                        //Don't want to show entire error of JSON reponse so we extract exact error message and show the user
                        Log.d(TAG, "Server response error");
                        ApiError error = ErrorUtils.parseError(response);
                        listener.onResponseError(error.getMessage() + ": " + error.getReason());

                    }
                }

                @Override
                public void onFailure(Call<AstronautResponse> call, Throwable t) {

                }


            });
        }

    }

    //Determines if device is connected to WiFi or mobile network. Does not guarantee device has internet access
    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }


}

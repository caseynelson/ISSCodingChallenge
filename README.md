This is the README for the ISS Coding Challenge

Author: Casey Nelson
Date: 2/7/2018
*******************
The basis for this project is to make a web service call to fetch data International Space Station API
and display the date/time/duration of each pass that is in your area (according to your GPS).

There were a few ways to architect the app (Do I use MVP, MVVM, Clean) and I ultimately just decided to separate the 
data retreival, networking and UI layers and worked with callbacks/listeners to respond to events.  

There is a viewpager that reuses the same fragment in both tabs and portrays parses a different response depending on which 
tab you choose. One fragment will show a list of the pass times adn the other will show the astronauts currently in the station.

With more time, I would add Espresso and Junit tests.